﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogHobbit.Appender;
using log4net.Config;
using log4net;

namespace LogHobbitAppenderTests
{
    [TestClass]
    public class LogHobbitAppenderTests
    {
        private static Boolean isFirstRun = true;
        private ILog logger = LogManager.GetLogger(typeof(LogHobbitAppenderTests));

        [TestInitialize]
        public void initialize()
        {
            if (isFirstRun)
            {
                XmlConfigurator.Configure();
            }
        }
        
        [TestMethod]
        public void BasicLogMessage()
        {
            logger.Info("This is a log message");
        }

        [TestMethod]
        public void LogException()
        {
            try
            {
                throw new Exception("This is a test exception");
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        [TestMethod]
        public void LogMessageAndException()
        {
            try
            {
                throw new Exception("Test exception");
            }
            catch (Exception e)
            {
                logger.Error("This is a message w/ Exception", e);
            }
        }
    }
}
