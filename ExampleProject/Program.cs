﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using LogHobbit.Appender;

namespace ExampleProject
{
    class Program
    {
        private static ILog logger = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            logger.Info("This is a log message");

            try
            {
                throw new Exception("This is a test exception");
            }
            catch(Exception e)
            {
                logger.Error(e);
            }

            try
            {
                throw new Exception("Test exception");
            }
            catch (Exception e)
            {
                logger.Error("This is a message w/ Exception", e);
            }
        }
    }
}
