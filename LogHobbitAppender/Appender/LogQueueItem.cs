﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogHobbit.Appender
{
    class LogQueueItem
    {
        /// <summary>
        /// The logging event
        /// </summary>
        public LoggingEvent LoggingEvent { get; set; }

        /// <summary>
        /// The event date the logging event occured at
        /// </summary>
        public DateTime EventDate { get; set; }
    }
}
