﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;
using System.Net;

namespace LogHobbit.Appender
{
    public class LogHobbitAppender : AppenderSkeleton
    {
        private const String JSON_FORMAT = @"
    ""accessKey"":""{0}"",
    ""date"":""{1}"",
    ""exception"":""{2}"",
    ""level"":""{3}"",
    ""logName"":""{4}"",
    ""message"":""{5}"",
    ""server"":""{6}""
";

        private Task executionTask = null;
        private Queue<LogQueueItem> eventQueue = new Queue<LogQueueItem>();
        private HttpClient client = new HttpClient();
        private String serverName;

        public Boolean IsAsync { get; set; }

        /// <summary>
        /// The access key used to validate the log group access
        /// </summary>
        public String AccessKey { get; set; }

        /// <summary>
        /// The url to the server to put the logs
        /// </summary>
        public String Server { get; set; }


        public LogHobbitAppender()
        {
            this.serverName = Environment.MachineName;
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            LogQueueItem queueItem = new LogQueueItem()
            {
                LoggingEvent = loggingEvent,
                EventDate = DateTime.Now
            };

            if (this.IsAsync)
            {
                lock (this.eventQueue)
                {
                    this.eventQueue.Enqueue(queueItem);

                    //
                    // Check to see if an execution task is active and start one if it is not
                    //
                    if (this.executionTask == null)
                    {
                        this.executionTask = new Task(() =>
                        {
                            this.uploadQueue();
                        });
                    }
                }
            }
            else
            {
                lock (this.eventQueue)
                {
                    this.eventQueue.Enqueue(queueItem);
                    this.uploadQueue();
                }
            }
        }

        private void uploadQueue()
        {
            try
            {
                int queueSize = 0;
                do
                {
                    //
                    // Get next item from queue
                    //
                    LogQueueItem queueItem = null;
                    lock (this.eventQueue)
                    {
                        if (this.eventQueue.Count > 0)
                        {
                            queueItem = this.eventQueue.Dequeue();
                        }
                        queueSize = this.eventQueue.Count;
                    }

                    //
                    // If an item was retrieved, process it
                    //
                    if (queueItem != null)
                    {
                        //
                        // Construct known logging level
                        //
                        String level;
                        if (queueItem.LoggingEvent.Level.Equals(Level.Warn))
                        {
                            level = "warn";
                        }
                        else if (queueItem.LoggingEvent.Level.Equals(Level.Error))
                        {
                            level = "error";
                        }
                        else if (queueItem.LoggingEvent.Level.Equals(Level.Error))
                        {
                            level = "fatal";
                        }
                        else
                        {
                            level = "info";
                        }

                        //
                        // Construct JSON message
                        //
                        StringBuilder json = new StringBuilder()
                                        .Append("{")
                                        .Append(@"""accessKey"":""").Append(this.AccessKey).Append(@""",")
                                        .Append(@"""date"":""").Append(queueItem.EventDate.ToString("yyyy-MM-dd'T'HH:mm:ss")).Append(@""",")
                                        .Append(@"""level"":""").Append(level).Append(@""",")
                                        .Append(@"""logName"":""").Append(queueItem.LoggingEvent.LoggerName).Append(@""",")
                                        .Append(@"""server"":""").Append(this.serverName).Append(@"""");

                        if (queueItem.LoggingEvent.MessageObject != null)
                        {
                            if(queueItem.LoggingEvent.MessageObject is Exception)
                            {
                                Exception exception = queueItem.LoggingEvent.MessageObject as Exception;
                                json.Append(@",""message"":""").Append(convertToJSONValue(exception.Message)).Append(@"""");
                                json.Append(@",""exception"":""").Append(convertToJSONValue(exceptionToString(exception))).Append(@"""");
                            }
                            else
                            {
                                json.Append(@",""message"":""").Append(convertToJSONValue(queueItem.LoggingEvent.MessageObject.ToString())).Append(@"""");
                            }
                        }
                        if (queueItem.LoggingEvent.ExceptionObject != null)
                        {
                            if (queueItem.LoggingEvent.MessageObject == null)
                            {
                                json.Append(@",""message"":""").Append(convertToJSONValue(queueItem.LoggingEvent.ExceptionObject.Message)).Append(@"""");
                            }

                            json.Append(@",""exception"":""").Append(convertToJSONValue(exceptionToString(queueItem.LoggingEvent.ExceptionObject))).Append(@"""");
                        }
                        json.Append("}");
                        
                        //
                        // Send Http rest call
                        //
                        HttpContent content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
                        Task<HttpResponseMessage> response = client.PutAsync(this.Server, content);
                        response.Wait();
                        if(response.Result.StatusCode != HttpStatusCode.OK)
                        {
                            Console.WriteLine("Could not write message to LogHobbit");
                        }
                    }
                } while (queueSize > 0);
            }
            catch(Exception e)
            {
                Console.WriteLine("An error occured while logging");
                Console.WriteLine(exceptionToString(e));
            }
            finally
            {
                this.executionTask = null;
            }
        }

        private static String convertToJSONValue(String content)
        {
            String retval = content.Replace("\\", "\\\\")
                            .Replace("\"", "\\\"")
                            .Replace(Environment.NewLine, "\\n");

            return retval;
        }

        private static String exceptionToString(Exception exception)
        {
            if (exception == null)
            {
                return null;
            }

            StringBuilder retval = new StringBuilder();
            retval.AppendLine(exception.ToString());
            
            if (exception.InnerException != null)
            {
                retval.AppendLine("caused by:");
                retval.AppendLine(exceptionToString(exception.InnerException));
            }
            
            return retval.ToString();
        }
    }
}
